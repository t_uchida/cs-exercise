theory Section2
imports Main
begin

fun add :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
  "add 0 n = n" |
  "add (Suc m) n = Suc (add m n)"

lemma add_assoc[simp]: "add (add m n) p = add m (add n p)"
  apply(induction m)
  apply(auto)
done

lemma add_n_0[simp]: "add n 0 = n"
  apply(induction n)
  apply(auto)
done

lemma add_m_Sn[simp]: "add m (Suc n) = Suc (add m n)"
  apply(induction m)
  apply(auto)
done

lemma add_comm[simp]: "add m n = add n m"
  apply(induction m)
  apply(auto)
done

fun double :: "nat \<Rightarrow> nat" where
  "double 0 = 0" |
  "double (Suc n) = Suc (Suc (double n))"

lemma ex_2_2: "double m = add m m"
  apply(induction m)
  apply(auto)
done

fun count :: "'a \<Rightarrow> 'a list \<Rightarrow> nat" where
  "count _ [] = 0" |
  "count a (x # xs) = (if a = x then 1 else 0) + count a xs"

lemma ex_2_3: "count x xs \<le> length xs"
  apply(induction xs)
  apply(auto)
done

fun snoc :: "'a list \<Rightarrow> 'a \<Rightarrow> 'a list" where
  "snoc [] a = [a]" |
  "snoc (x # xs) a = x # snoc xs a"

fun reverse :: "'a list \<Rightarrow> 'a list" where
  "reverse [] = []" |
  "reverse (x # xs) = snoc (reverse xs) x"

lemma rev_snoc[simp]: "reverse (snoc xs x) = x # reverse xs"
  apply(induction xs)
  apply(auto)
done

lemma ex_2_4: "reverse (reverse xs) = xs"
  apply(induction xs)
  apply(auto)
done

fun sum_upto :: "nat \<Rightarrow> nat" where
  "sum_upto 0 = 0" |
  "sum_upto n = n + sum_upto (n - 1)"

lemma ex_2_5: "sum_upto n = n * (n + 1) div 2"
  apply(induction n)
  apply(auto)
done

datatype 'a tree = Tip | Node "'a tree" 'a "'a tree"

fun contents :: "'a tree \<Rightarrow> 'a list" where
  "contents Tip = []" |
  "contents (Node t1 a t2) = contents t1 @ a # contents t2"

fun sum_tree :: "nat tree \<Rightarrow> nat" where
  "sum_tree Tip = 0" |
  "sum_tree (Node t1 a t2) = sum_tree t1 + a + sum_tree t2"

lemma ex_2_6: "sum_tree t = sum_list (contents t)"
  apply(induction t)
  apply(auto)
done

datatype 'a tree2 = Tip2 | Leaf2 'a | Node2 "'a tree2" "'a tree2"

fun mirror :: "'a tree2 \<Rightarrow> 'a tree2" where
  "mirror Tip2 = Tip2" |
  "mirror (Leaf2 a) = Leaf2 a" |
  "mirror (Node2 t1 t2) = Node2 (mirror t2) (mirror t1)"

fun pre_order :: "'a tree2 \<Rightarrow> 'a list" where
  "pre_order Tip2 = []" |
  "pre_order (Leaf2 a) = [a]" |
  "pre_order (Node2 t1 t2) = pre_order t1 @ pre_order t2"

fun post_order :: "'a tree2 \<Rightarrow> 'a list" where
  "post_order Tip2 = []" |
  "post_order (Leaf2 a) = [a]" |
  "post_order (Node2 t1 t2) = post_order t2 @ post_order t1"

lemma ex_2_7: "pre_order (mirror t) = post_order t"
  apply(induction t)
  apply(auto)
done

fun intersperse :: "'a \<Rightarrow> 'a list \<Rightarrow> 'a list" where
  "intersperse _ [] = []" |
  "intersperse _ [x] = [x]" |
  "intersperse a (x # xs) = x # a # intersperse a xs"

lemma ex_2_8: "map f (intersperse a xs) = intersperse (f a) (map f xs)"
  apply(induction xs rule: intersperse.induct)
  apply(auto)
done

fun itadd :: "nat \<Rightarrow> nat \<Rightarrow> nat" where
  "itadd 0 n = n" |
  "itadd (Suc m) n = itadd m (Suc n)"

lemma ex_2_9: "itadd m n = add m n"
  apply(induction m arbitrary: n)
  apply(auto)
done

datatype tree0 = Tip0 | Node0 tree0 tree0

fun nodes :: "tree0 \<Rightarrow> nat" where
  "nodes Tip0 = 0" |
  "nodes (Node0 t1 t2) = nodes t1 + nodes t2"

fun explode :: "nat \<Rightarrow> tree0 \<Rightarrow> tree0" where
  "explode 0 t = t" |
  "explode (Suc n) t = explode n (Node0 t t)"

lemma ex_2_10: "nodes (explode n t) = nodes t * 2 ^ n"
  apply(induction n arbitrary: t)
  apply(auto)
done

datatype exp = Var | Const int | Add exp exp | Mult exp exp

fun eval :: "exp \<Rightarrow> int \<Rightarrow> int" where
  "eval Var x = x" |
  "eval (Const i) _ = i" |
  "eval (Add e1 e2) x = eval e1 x + eval e2 x" |
  "eval (Mult e1 e2) x = eval e1 x * eval e2 x"

fun evalp :: "int list \<Rightarrow> int \<Rightarrow> int" where
  "evalp [] _ = 0" |
  "evalp (x # xs) n = x + n * evalp xs n"

fun addp :: "int list \<Rightarrow> int list \<Rightarrow> int list" where
  "addp [] xs = xs" |
  "addp xs [] = xs" |
  "addp (x # xs) (y # ys) = (x + y) # addp xs ys"

fun multp :: "int list \<Rightarrow> int list \<Rightarrow> int list" where
  "multp [] _ = []" |
  "multp (x # xs) ys = addp (map (\<lambda>y. x * y) ys) (0 # multp xs ys)"

fun coeffs :: "exp \<Rightarrow> int list" where
  "coeffs Var = [0, 1]" |
  "coeffs (Const i) = [i]" |
  "coeffs (Add e1 e2) = addp (coeffs e1) (coeffs e2)" |
  "coeffs (Mult e1 e2) = multp (coeffs e1) (coeffs e2)"

lemma coeff_preserves_addp:
   "evalp (coeffs e1) x = eval e1 x \<Longrightarrow>
    evalp (coeffs e2) x = eval e2 x \<Longrightarrow>
    evalp (addp (coeffs e1) (coeffs e2)) x = eval e1 x + eval e2 x"
  sorry

lemma coeff_preserves_multp:
   "evalp (coeffs e1) x = eval e1 x \<Longrightarrow>
    evalp (coeffs e2) x = eval e2 x \<Longrightarrow>
    evalp (multp (coeffs e1) (coeffs e2)) x = eval e1 x * eval e2 x"
  sorry

lemma ex_2_11: "evalp (coeffs e) x = eval e x"
  apply(induction e)
  apply(auto simp add: coeff_preserves_addp coeff_preserves_multp)
done

end