theory Section3
imports Main
begin

type_synonym vname = string
datatype aexp = N int | V vname | Plus aexp aexp

type_synonym val = int
type_synonym state = "vname \<Rightarrow> int"

fun aval :: "aexp \<Rightarrow> state \<Rightarrow> val" where
  "aval (N n) s = n" |
  "aval (V x) s = s x" |
  "aval (Plus a1 a2) s = aval a1 s + aval a2 s"

fun asimp_const :: "aexp \<Rightarrow> aexp" where
  "asimp_const (N n) = N n" |
  "asimp_const (V x) = V x" |
  "asimp_const (Plus a1 a2) =
    (case (asimp_const a1, asimp_const a2) of
      (N n1, N n2) \<Rightarrow> N (n1 + n2) |
      (b1, b2) \<Rightarrow> Plus b1 b2)"

fun plus :: "aexp \<Rightarrow> aexp \<Rightarrow> aexp" where
  "plus (N i1) (N i2) = N (i1 + i2)" |
  "plus (N i) a = (if i = 0 then a else Plus (N i) a)" |
  "plus a (N i) = (if i = 0 then a else Plus a (N i))" |
  "plus a1 a2 = Plus a1 a2"

fun asimp :: "aexp \<Rightarrow> aexp" where
  "asimp (N n) = N n" |
  "asimp (V x) = V x" |
  "asimp (Plus a1 a2) = plus (asimp a1) (asimp a2)"

fun optimal :: "aexp \<Rightarrow> bool" where
  "optimal (Plus (N _) (N _)) = False" |
  "optimal (Plus a1 a2) = (optimal a1 & optimal a2)" |
  "optimal _ = True"

lemma ex_3_1 : "optimal (asimp_const a)"
  apply(induction a)
  apply(auto split: aexp.split)
done

fun subst :: "vname \<Rightarrow> aexp \<Rightarrow> aexp \<Rightarrow> aexp" where
  "subst _ _ (N n) = N n" |
  "subst x a (V y) = (if x = y then a else (V y))" |
  "subst x a (Plus a1 a2) = Plus (subst x a a1) (subst x a a2)"

lemma ex_3_3 : "aval a1 s = aval a2 s \<Longrightarrow> aval (subst x a1 e) s = aval (subst x a2 e) s"
  apply(induction e)
  apply(auto)
done

datatype aexp2 = N2 int | V2 vname | Plus2 aexp2 aexp2 | Times2 aexp2 aexp2

fun aval2 :: "aexp2 \<Rightarrow> state \<Rightarrow> val" where
  "aval2 (N2 n) s = n" |
  "aval2 (V2 x) s = s x" |
  "aval2 (Plus2 a1 a2) s = aval2 a1 s + aval2 a2 s" |
  "aval2 (Times2 a1 a2) s = aval2 a1 s * aval2 a2 s"

fun plus2 :: "aexp2 \<Rightarrow> aexp2 \<Rightarrow> aexp2" where
  "plus2 (N2 i1) (N2 i2) = N2 (i1 + i2)" |
  "plus2 (N2 i) a = (if i = 0 then a else Plus2 (N2 i) a)" |
  "plus2 a (N2 i) = (if i = 0 then a else Plus2 a (N2 i))" |
  "plus2 a1 a2 = Plus2 a1 a2"

fun times :: "aexp2 \<Rightarrow> aexp2 \<Rightarrow> aexp2" where
  "times (N2 i1) (N2 i2) = N2 (i1 * i2)" |
  "times (N2 i) a = (if i = 0 then N2 0 else if i = 1 then a else Times2 (N2 i) a)" |
  "times a (N2 i) = (if i = 0 then N2 0 else if i = 1 then a else Times2 a (N2 i))" |
  "times a1 a2 = Times2 a1 a2"

fun asimp2 :: "aexp2 \<Rightarrow> aexp2" where
  "asimp2 (N2 n) = N2 n" |
  "asimp2 (V2 x) = V2 x" |
  "asimp2 (Plus2 a1 a2) = plus2 (asimp2 a1) (asimp2 a2)" |
  "asimp2 (Times2 a1 a2) = times (asimp2 a1) (asimp2 a2)"

lemma ex_3_4_1 : "aval2 (plus2 a1 a2) s = aval2 a1 s + aval2 a2 s"
  apply(induction a1 a2 rule: plus2.induct)
  apply(auto)
done

lemma ex_3_4_2 : "aval2 (times a1 a2) s = aval2 a1 s * aval2 a2 s"
  apply(induction a1 a2 rule: times.induct)
  apply(auto)
done

lemma ex_3_4_3 : "aval2 (asimp2 a) s = aval2 a s"
  apply(induction a)
  apply(auto simp add: ex_3_4_1 ex_3_4_2)
done

datatype lexp = Nl int | Vl vname | Plusl lexp lexp | LET vname lexp lexp

fun lval :: "lexp \<Rightarrow> state \<Rightarrow> int" where
  "lval (Nl n) s = n" |
  "lval (Vl x) s = s x" |
  "lval (Plusl a1 a2) s = lval a1 s + lval a2 s" |
  "lval (LET x a1 a2) s = lval a2 (s (x := lval a1 s))"

fun inline :: "lexp \<Rightarrow> aexp" where
  "inline (Nl n) = N n" |
  "inline (Vl x) = V x" |
  "inline (Plusl a1 a2) = Plus (inline a1) (inline a2)" |
  "inline (LET x a1 a2) = subst x (inline a1) (inline a2)"

lemma subst_lemma: "aval (subst x a e) s = aval e (s (x := aval a s))"
  apply(induction e)
  apply(auto)
  done

lemma ex_3_6 : "aval (inline e) s = lval e s"
  apply(induction e arbitrary: s)
  apply(auto simp add: subst_lemma)
done

datatype bexp = Bc bool | Not bexp | And bexp bexp | Less aexp aexp

fun bval :: "bexp \<Rightarrow> state \<Rightarrow> bool" where
  "bval (Bc v) s = v" |
  "bval (Not b) s = (\<not> bval b s)" |
  "bval (And b1 b2) s = (bval b1 s \<and> bval b2 s)" |
  "bval (Less a1 a2) s = (aval a1 s < aval a2 s)"

fun not :: "bexp \<Rightarrow> bexp" where
  "not (Bc True) = Bc False" |
  "not (Bc False) = Bc True" |
  "not b = Not b"

fun "and" :: "bexp \<Rightarrow> bexp \<Rightarrow> bexp" where
  "and (Bc True) b = b" |
  "and b (Bc True) = b" |
  "and (Bc False) b = Bc False" |
  "and b (Bc False) = Bc False" |
  "and b1 b2 = And b1 b2"

fun less :: "aexp \<Rightarrow> aexp \<Rightarrow> bexp" where
  "less (N n1) (N n2) = Bc (n1 < n2)" |
  "less a1 a2 = Less a1 a2"

fun bsimp :: "bexp \<Rightarrow> bexp" where
  "bsimp (Bc v) = Bc v" |
  "bsimp (Not b) = not (bsimp b)" |
  "bsimp (And b1 b2) = and (bsimp b1) (bsimp b2)" |
  "bsimp (Less a1 a2) = less (asimp a1) (asimp a2)"

datatype instr = LOADI val | LOAD vname | ADD
type_synonym stack = "val list"

fun exec1 :: "instr \<Rightarrow> state \<Rightarrow> stack \<Rightarrow> stack" where
  "exec1 (LOADI n) _ stk = n # stk" |
  "exec1 (LOAD x) s stk = s x # stk" |
  "exec1 ADD _ (j # i # stk) = (i + j) # stk"

fun exec :: "instr list \<Rightarrow> state \<Rightarrow> stack \<Rightarrow> stack" where
  "exec [] _ stk = stk" |
  "exec (i # is) s stk = exec is s (exec1 i s stk)"

fun comp :: "aexp \<Rightarrow> instr list" where
  "comp (N n) = [LOADI n]" |
  "comp (V x) = [LOAD x]" |
  "comp (Plus e1 e2) = comp e1 @ comp e2 @ [ADD]"

lemma exec_join : "exec (is1 @ is2) s stk = exec is2 s (exec is1 s stk)"
  apply(induction is1 arbitrary: stk)
  apply(auto)
done

lemma exec_comp : "exec (comp a) s stk = aval a s # stk"
  apply(induction a arbitrary: stk)
  apply(auto simp add: exec_join)
done

fun exec1x :: "instr \<Rightarrow> state \<Rightarrow> stack \<Rightarrow> stack option" where
  "exec1x (LOADI n) _ stk = Some (n # stk)" |
  "exec1x (LOAD x) s stk = Some (s x # stk)" |
  "exec1x ADD _ (j # i # stk) = Some ((i + j) # stk)" |
  "exec1x _ _ _ = None"

fun execx :: "instr list \<Rightarrow> state \<Rightarrow> stack \<Rightarrow> stack option" where
  "execx [] _ stk = Some stk" |
  "execx (i # is) s stk =
    (case exec1x i s stk of
      None \<Rightarrow> None |
      Some stk' \<Rightarrow> execx is s stk')"

lemma execx_join : "execx is1 s stk = Some stk' \<Longrightarrow> execx (is1 @ is2) s stk = execx is2 s stk'"
  apply(induction is1 arbitrary: stk)
  apply(auto split: option.split)
done

lemma ex_3_10 : "execx (comp a) s stk = Some (aval a s # stk)"
  apply(induction a arbitrary: stk)
  apply(auto simp add:execx_join)
done

type_synonym reg = nat
datatype instr2 = LDI int reg | LD vname reg | ADD2 reg reg

fun exec1y :: "instr2 \<Rightarrow> state \<Rightarrow> (reg \<Rightarrow> int) \<Rightarrow> reg \<Rightarrow> int" where
  "exec1y (LDI i r) s rs = rs (r := i)" |
  "exec1y (LD v r) s rs = rs (r := s v)" |
  "exec1y (ADD2 r1 r2) s rs = rs (r1 := rs r1 + rs r2)"

fun execy :: "instr2 list \<Rightarrow> state \<Rightarrow> (reg \<Rightarrow> int) \<Rightarrow> reg \<Rightarrow> int" where
  "execy [] _ rs = rs" |
  "execy (i # is) s rs = execy is s (exec1y i s rs)"

fun compr :: "aexp \<Rightarrow> reg \<Rightarrow> instr2 list" where
  "compr (N n) r = [LDI n r]" |
  "compr (V x) r = [LD x r]" |
  "compr (Plus e1 e2) r = compr e1 r @ compr e2 (r + 1) @ [ADD2 r (r + 1)]"

lemma execy_join : "execy (is1 @ is2) s stk = execy is2 s (execy is1 s stk)"
  apply(induction is1 arbitrary: stk)
  apply(auto)
done

lemma execy_left_alone : "r < r' \<Longrightarrow> rs r = v \<Longrightarrow> execy (compr a r') s rs r = v"
  apply(induction a arbitrary: r r' rs)
  apply(auto simp add: execy_join)
done

lemma ex_3_11 : "execy (compr a r) s rs r = aval a s"
  apply(induction a arbitrary: r rs)
  apply(auto simp add: execy_join execy_left_alone)
done

end