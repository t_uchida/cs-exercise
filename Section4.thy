theory Section4
imports Main
begin

datatype 'a tree = Tip | Node "'a tree" 'a "'a tree"

fun set :: "'a tree \<Rightarrow> 'a set" where
  "set Tip = {}" |
  "set (Node t1 a t2) = set t1 \<union> {a} \<union> set t2"

fun ord :: "int tree \<Rightarrow> bool" where
  "ord Tip = True" |
  "ord (Node t1 a t2) = (ord t1 \<and> ord t2 \<and> (\<forall>x \<in> set t1. x < a) \<and> (\<forall>x \<in> set t2. a \<le> x))"

fun ins :: "int \<Rightarrow> int tree \<Rightarrow> int tree" where
  "ins x Tip = Node Tip x Tip" |
  "ins x (Node t1 a t2) = (if x < a then (Node (ins x t1) a t2) else (Node t1 a (ins x t2)))"

lemma ex_4_1_1 : "set (ins x t) = {x} \<union> set t"
  apply(induction t)
  apply(auto)
done

lemma ex_4_1_2 : "ord t \<Longrightarrow> ord (ins i t)"
  apply(induction t)
  apply(auto simp add: ex_4_1_1)
done

inductive palindrome :: "'a list \<Rightarrow> bool" where
  par0: "palindrome []" |
  par1: "palindrome [x]" |
  parN: "palindrome xs \<Longrightarrow> palindrome (x # xs @ [x])"

lemma ex_4_2 : "palindrome xs \<Longrightarrow> rev xs = xs"
  apply(induction xs rule: palindrome.induct)
  apply(auto)
done

inductive star :: "('a \<Rightarrow> 'a \<Rightarrow> bool) \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" for r where
  refl: "star r x x" |
  step: "r x y \<Longrightarrow> star r y z \<Longrightarrow> star r x z"

lemma star_unit : "r x y \<Longrightarrow> star r x y"
  apply(auto simp add: refl step)
done

lemma star_assoc : "star r x y \<Longrightarrow> star r y z \<Longrightarrow> star r x z"
  apply(induction rule: star.induct)
  apply(auto simp add: step)
done

lemma star_recip_step : "star r x y \<Longrightarrow> r y z \<Longrightarrow> star r x z"
  apply(auto simp add: star_unit star_assoc)
done

inductive star' :: "('a \<Rightarrow> 'a \<Rightarrow> bool) \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" for r where
  refl': "star' r x x" |
  step': "star' r x y \<Longrightarrow> r y z \<Longrightarrow> star' r x z"

lemma star'_unit : "r x y \<Longrightarrow> star' r x y"
  by (meson refl' step')

lemma star'_assoc : "star' r x y \<Longrightarrow> star' r y z \<Longrightarrow> star' r x z"
  sorry

lemma star'_recip_step' : "r x y \<Longrightarrow> star' r y z \<Longrightarrow> star' r x z"
  sorry

lemma ex_4_3_1 : "star' r x y \<Longrightarrow> star r x y"
  apply(induction rule: star'.induct)
  apply(auto simp add: refl star_recip_step)
done

lemma ex_4_3_2 : "star r x y \<Longrightarrow> star' r x y"
  apply(induction rule: star.induct)
  apply(auto simp add: refl' star'_recip_step')
done

inductive iter :: "('a \<Rightarrow> 'a \<Rightarrow> bool) \<Rightarrow> nat \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" where
  irefl : "iter r 0 x x" |
  istep : "r x y \<Longrightarrow> iter r n y z \<Longrightarrow> iter r (Suc n) x z"

lemma ex_4_4 : "star r x y \<Longrightarrow> \<exists>n. iter r n x y"
  apply(induction rule: star.induct)
  apply(meson irefl)
  by (meson istep)

end