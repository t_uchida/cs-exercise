theory Section5
imports Main
begin

lemma ex_5_1:
  assumes T: "\<forall>x y. T x y \<or> T y x"
  and A: "\<forall>x y. A x y \<and> A y x \<longrightarrow> x = y"
  and TA: "\<forall>x y. T x y \<longrightarrow> A x y"
  and PA: "A x y"
  shows "T x y"
proof -
  have "T x y \<or> T y x" using T by simp
  thus "T x y"
  proof
    assume "T x y"
    thus "T x y" by simp
  next
    assume 1: "T y x"
    hence "A y x" using TA by simp
    hence "x = y" using A PA by simp
    thus "T x y" using 1 by simp
  qed
qed

lemma ex_5_2: "\<exists>ys zs. xs = ys @ zs \<and> (length ys = length zs \<or> length ys = length zs + 1)" (is "\<exists> ys. ?P ys")
proof
  show "?P (take ((length xs + 1) div 2) xs)" (is "\<exists> zs. ?Q zs")
  proof
    show "?Q (drop ((length xs + 1) div 2) xs)" by auto
  qed
qed

inductive ev :: "nat \<Rightarrow> bool" where
  ev0: "ev 0" |
  evSS: "ev n \<Longrightarrow> ev (Suc (Suc n))"

lemma ex_5_3: "ev (Suc (Suc n)) \<Longrightarrow> ev n"
proof (induction "Suc (Suc n)" arbitrary: n rule: ev.induct)
  case evSS
  thus ?case by simp
qed

lemma ex_5_4: "\<not> ev (Suc (Suc (Suc 0)))" (is "\<not> ?P")
proof
  assume ?P
  thus False
  proof cases
    case evSS
    assume "ev (Suc 0)"
    thus False
    proof cases
    qed
  qed
qed

inductive star :: "('a \<Rightarrow> 'a \<Rightarrow> bool) \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" for r where
  refl: "star r x x" |
  step: "r x y \<Longrightarrow> star r y z \<Longrightarrow> star r x z"

lemma star_unit : "r x y \<Longrightarrow> star r x y"
  apply(auto simp add: refl step)
done

lemma star_assoc : "star r x y \<Longrightarrow> star r y z \<Longrightarrow> star r x z"
  apply(induction rule: star.induct)
  apply(auto simp add: step)
done

lemma star_recip_step : "star r x y \<Longrightarrow> r y z \<Longrightarrow> star r x z"
  apply(auto simp add: star_unit star_assoc)
done

inductive iter :: "('a \<Rightarrow> 'a \<Rightarrow> bool) \<Rightarrow> nat \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" where
  irefl : "iter r 0 x x" |
  istep : "r x y \<Longrightarrow> iter r n y z \<Longrightarrow> iter r (Suc n) x z"

lemma ex_5_5: "iter r n x y \<Longrightarrow> star r x y"
proof (induction rule: iter.induct)
  case (irefl r x)
  then show ?case by (simp add: star.refl)
next
  case (istep r x y n z)
  then show ?case by (meson star.step)
qed

fun elems :: "'a list \<Rightarrow> 'a set" where
  "elems [] = {}" |
  "elems (x # xs) = {x} \<union> elems xs"

lemma ex_5_6: "x \<in> elems xs \<Longrightarrow> \<exists> ys zs. xs = ys @ x # zs \<and> x \<notin> elems ys"
proof (induction rule: elems.induct)
  case 1
  thus ?case by simp
next
  case (2 a as)
  assume IH1: "x \<in> elems as \<Longrightarrow> \<exists>ys zs. as = ys @ x # zs \<and> x \<notin> elems ys"
  assume 5: "x \<in> elems (a # as)"
  thus ?case
  proof (cases "x = a")
    case True
    assume 3: "x = a"
    show "\<exists>ys zs. a # as = ys @ x # zs \<and> x \<notin> elems ys" (is "\<exists>ys. ?P ys")
    proof
      show "?P []" (is "\<exists>zs. ?Q zs")
      proof
        show "?Q as" using 3 by simp
      qed
    qed
  next
    case False
    assume 4: "x \<noteq> a"
    hence "x \<in> elems as" using 4 5 by simp
    then obtain vs ws where IH2: "as = vs @ x # ws \<and> x \<notin> elems vs" using IH1 by auto
    show "\<exists>ys zs. a # as = ys @ x # zs \<and> x \<notin> elems ys" (is "\<exists>ys. ?P ys")
    proof
      show "?P (a # vs)" (is "\<exists> zs. ?Q zs")
      proof
        show "?Q ws" by (simp add: False IH2)
      qed
    qed
  qed
qed

end