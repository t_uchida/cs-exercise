theory Section7
imports Main
begin

type_synonym vname = string
type_synonym val = int
type_synonym state = "vname \<Rightarrow> int"

datatype aexp = N int | V vname | Plus aexp aexp
datatype bexp = Bc bool | Not bexp | And bexp bexp | Less aexp aexp

fun aval :: "aexp \<Rightarrow> state \<Rightarrow> val" where
  "aval (N n) s = n" |
  "aval (V x) s = s x" |
  "aval (Plus a1 a2) s = aval a1 s + aval a2 s"

fun plus :: "aexp \<Rightarrow> aexp \<Rightarrow> aexp" where
  "plus (N i1) (N i2) = N (i1 + i2)" |
  "plus (N i) a = (if i = 0 then a else Plus (N i) a)" |
  "plus a (N i) = (if i = 0 then a else Plus a (N i))" |
  "plus a1 a2 = Plus a1 a2"

lemma aval_plus[simp]: "aval (plus a1 a2) s = aval a1 s + aval a2 s"
  apply(induction a1 a2 rule: plus.induct)
  apply(auto)
done

fun asimp :: "aexp \<Rightarrow> aexp" where
  "asimp (N n) = N n" |
  "asimp (V x) = V x" |
  "asimp (Plus a1 a2) = plus (asimp a1) (asimp a2)"

lemma aval_simp[simp]: "aval (asimp a) s = aval a s"
  apply(induction a)
  apply(auto)
done

fun bval :: "bexp \<Rightarrow> state \<Rightarrow> bool" where
  "bval (Bc v) s = v" |
  "bval (Not b) s = (\<not> bval b s)" |
  "bval (And b1 b2) s = (bval b1 s \<and> bval b2 s)" |
  "bval (Less a1 a2) s = (aval a1 s < aval a2 s)"

fun less :: "aexp \<Rightarrow> aexp \<Rightarrow> bexp" where
  "less (N n1) (N n2) = Bc (n1 < n2)" |
  "less a1 a2 = Less a1 a2"

lemma bval_less[simp]: "bval (less a1 a2) s = (aval a1 s < aval a2 s)"
  apply(induction a1 a2 rule: less.induct)
  apply(auto)
done

fun "and" :: "bexp \<Rightarrow> bexp \<Rightarrow> bexp" where
  "and (Bc True) b = b" |
  "and b (Bc True) = b" |
  "and (Bc False) b = Bc False" |
  "and b (Bc False) = Bc False" |
  "and b1 b2 = And b1 b2"

lemma bval_and[simp]: "bval (and b1 b2) s = (bval b1 s \<and> bval b2 s)"
  apply(induction b1 b2 rule: and.induct)
  apply(auto)
done

fun "not" :: "bexp \<Rightarrow> bexp" where
  "not (Bc True) = Bc False" |
  "not (Bc False) = Bc True" |
  "not b = Not b"

lemma bval_not[simp]: "bval (not b) s = (\<not> bval b s)"
  apply(induction b rule: not.induct)
  apply(auto)
done

datatype com
  = SKIP
  | Assign vname aexp ("_ ::= _" [1000, 61] 61)
  | Seq com com       ("_;;/ _" [60, 61] 60)
  | If bexp com com   ("(IF _/ THEN _/ ELSE _/)" [0, 0, 61] 61)
  | While bexp com    ("(WHILE _/ DO _)" [0, 61] 61)

inductive big_step :: "com \<times> state \<Rightarrow> state \<Rightarrow> bool" (infix "\<Rightarrow>" 55) where
  Skip:       "(SKIP, s) \<Rightarrow> s" |
  Assign:     "(x ::= a, s) \<Rightarrow> s (x := aval a s)" |
  Seq:        "\<lbrakk> (c1, s1) \<Rightarrow> s2; (c2, s2) \<Rightarrow> s3 \<rbrakk> \<Longrightarrow> (c1;; c2, s1) \<Rightarrow> s3" |
  IfTrue:     "\<lbrakk> bval b s; (c1, s) \<Rightarrow> t \<rbrakk> \<Longrightarrow> (IF b THEN c1 ELSE c2, s) \<Rightarrow> t" |
  IfFalse:    "\<lbrakk> \<not> bval b s; (c2, s) \<Rightarrow> t \<rbrakk> \<Longrightarrow> (IF b THEN c1 ELSE c2, s) \<Rightarrow> t" |
  WhileFalse: "\<not> bval b s \<Longrightarrow> (WHILE b DO c, s) \<Rightarrow> s" |
  WhileTrue:  "\<lbrakk> bval b s1; (c, s1) \<Rightarrow> s2; (WHILE b DO c, s2) \<Rightarrow> s3 \<rbrakk> \<Longrightarrow> (WHILE b DO c, s1) \<Rightarrow> s3"

code_pred big_step .

declare big_step.intros [intro]

lemmas big_step_induct = big_step.induct[split_format(complete)]

inductive_cases SkipE[elim!]:   "(SKIP, s) \<Rightarrow> t"
inductive_cases AssignE[elim!]: "(x ::= a, s) \<Rightarrow> t"
inductive_cases SeqE[elim!]:    "(c1;; c2, s1) \<Rightarrow> s3"
inductive_cases IfE[elim!]:     "(IF b THEN c1 ELSE c2, s) \<Rightarrow> t"
inductive_cases WhileE[elim!]:  "(WHILE b DO c, s) \<Rightarrow> t"

lemma "(IF b THEN SKIP ELSE SKIP, s) \<Rightarrow> t \<Longrightarrow> t = s" by auto

lemma lm_7_2: "(c1;; c2;; c3, s) \<Rightarrow> s' \<longleftrightarrow> (c1;; (c2;; c3), s) \<Rightarrow> s'" by blast

abbreviation equiv_c :: "com \<Rightarrow> com \<Rightarrow> bool" (infix "\<sim>" 50) where
  "c \<sim> c' \<equiv> (\<forall> s t. (c, s) \<Rightarrow> t = (c', s) \<Rightarrow> t)"

lemma lm_7_3: "WHILE b DO c \<sim> IF b THEN c;; WHILE b DO c ELSE SKIP" sorry

lemma lm_7_4: "IF b THEN c ELSE c \<sim> c" by auto

lemma sim_refl:  "c \<sim> c" by simp
lemma sim_sym:   "(c \<sim> c') = (c' \<sim> c)" by blast
lemma sim_trans: "c \<sim> c' \<Longrightarrow> c' \<sim> c'' \<Longrightarrow> c \<sim> c''" by blast

lemma lm_7_9: "\<lbrakk> (c, s) \<Rightarrow> t; (c, s) \<Rightarrow> t' \<rbrakk> \<Longrightarrow> t' = t" sorry

inductive star :: "('a \<Rightarrow> 'a \<Rightarrow> bool) \<Rightarrow> 'a \<Rightarrow> 'a \<Rightarrow> bool" for r where
  refl: "star r x x" |
  step: "r x y \<Longrightarrow> star r y z \<Longrightarrow> star r x z"

hide_fact (open) refl step

lemma star_trans: "star r x y \<Longrightarrow> star r y z \<Longrightarrow> star r x z"
proof(induction rule: star.induct)
  case refl thus ?case .
next
  case step thus ?case by (auto simp add: star.step)
qed

lemmas star_induct = star.induct[of "r :: 'a \<times> 'b \<Rightarrow> 'a \<times> 'b \<Rightarrow> bool", split_format(complete)]

declare star.refl[simp, intro]

lemma star_step1[simp, intro]: "r x y \<Longrightarrow> star r x y" by (auto simp add: star.step)

code_pred star .

inductive small_step :: "com \<times> state \<Rightarrow> com \<times> state \<Rightarrow> bool" (infix "\<rightarrow>" 55) where
  Assign: "(x ::= a, s) \<rightarrow> (SKIP, s (x := aval a s))" |
  Seq1: "(SKIP;; c2, s) \<rightarrow> (c2, s)" |
  Seq2: "(c1, s) \<rightarrow> (c1', s') \<Longrightarrow> (c1;; c2, s) \<rightarrow> (c1';; c2, s')" |
  IfTrue: "bval b s \<Longrightarrow> (IF b THEN c1 ELSE c2, s) \<rightarrow> (c1, s)" |
  IfFalse: "\<not> bval b s \<Longrightarrow> (IF b THEN c1 ELSE c2, s) \<rightarrow> (c2, s)" |
  While: "(WHILE b DO c, s) \<rightarrow> (IF b THEN (c;; WHILE b DO c) ELSE SKIP, s)"

abbreviation small_steps :: "com \<times> state \<Rightarrow> com \<times> state \<Rightarrow> bool" (infix "\<rightarrow>*" 55) where
  "x \<rightarrow>* y == star small_step x y"

code_pred small_step .

lemmas small_step_induct = small_step.induct[split_format(complete)]

declare small_step.intros[simp, intro]

inductive_cases SSkipE[elim!]: "(SKIP, s) \<rightarrow> ct"
inductive_cases SAssignE[elim!]: "(x ::= a, s) \<rightarrow> ct"
inductive_cases SSeqE[elim!]: "(c1;; c2, s) \<rightarrow> ct"
inductive_cases SIfE[elim!]: "(IF b THEN c1 ELSE c2, s) \<rightarrow> ct"
inductive_cases SWhileE[elim!]: "(WHILE b DO c, s) \<rightarrow> ct"

lemma deterministic: "cs \<rightarrow> cs' \<Longrightarrow> cs \<rightarrow> cs'' \<Longrightarrow> cs'' = cs'"
  apply(induction arbitrary: cs'' rule: small_step.induct)
  apply blast+
done

lemma star_seq2: "(c1, s) \<rightarrow>* (c1', s') \<Longrightarrow> (c1;; c2, s) \<rightarrow>* (c1';;c2, s')"
proof(induction rule: star_induct)
  case refl thus ?case by simp
next
  case step
  thus ?case by (metis Seq2 star.step)
qed

lemma seq_comp: "\<lbrakk> (c1, s1) \<rightarrow>* (SKIP, s2); (c2, s2) \<rightarrow>* (SKIP, s3) \<rbrakk> \<Longrightarrow> (c1;; c2, s1) \<rightarrow>* (SKIP, s3)"
  by(blast intro: star.step star_seq2 star_trans)

end